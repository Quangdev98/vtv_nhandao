

$(document).ready(function () {

	var langArray = [];
	$('.vodiapicker option').each(function(){
	  var img = $(this).attr("data-thumbnail");
	  var text = this.innerText;
	  var value = $(this).val();
	  var item = '<li><img src="'+ img +'" alt="" value="'+value+'"/><span>'+ text +'</span><i class="fal fa-angle-down"></i></li>';
	  langArray.push(item);
	})
	
	$('#list-select').html(langArray);
	
	$('.btn-select').html(langArray[0]);
	$('.btn-select').attr('value', 'en');
	
	$('#list-select li').click(function(){
	   var img = $(this).find('img').attr("src");
	   var value = $(this).find('img').attr('value');
	   var text = this.innerText;
	   var item = '<li><img src="'+ img +'" alt="" /><span>'+ text +'</span><i class="fal fa-angle-down"></i></li>';
	  $('.btn-select').html(item);
	  $('.btn-select').attr('value', value);
	  $(".wrap-select").toggle();
	});
	
	$(".btn-select").click(function(){
			$(".wrap-select").toggle();
		});

	var sessionLang = localStorage.getItem('lang');
	if (sessionLang){
	  var langIndex = langArray.indexOf(sessionLang);
	  $('.btn-select').html(langArray[langIndex]);
	  $('.btn-select').attr('value', sessionLang);
	} else {
	   var langIndex = langArray.indexOf('ch');
	  console.log(langIndex);
	  $('.btn-select').html(langArray[langIndex]);
	  //$('.btn-select').attr('value', 'en');
	}

	// mega menu 
	$('.header-megamenu>.btn').click(function() {
		$(this).toggleClass('active')
		$(this).siblings('.header-mega').toggleClass('show-menu')
		$("body").toggleClass('openmn')
	})
	$(document).mouseup(function (e) {
		var container = $(".header-megamenu");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$('.header-megamenu>.btn').removeClass('active')
				$('.header-megamenu .header-mega').removeClass('show-menu')
				$("body").removeClass('openmn')
		}
	});

	// 
	$(".dropdown .icon").click(function() {
		$(this).parents(".dropdown").toggleClass('open')
	})
	$(document).mouseup(function (e) {
		var container = $(".list-video-cate .dropdown");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
				$(".list-video-cate .dropdown").removeClass('open')
		}
	});
	$(".choose-bottom.home-fv-list .choose-play-video").click(function(event){
		if($(this).hasClass('active')){
			event. preventDefault();
		} else {
			$(".choose-bottom.home-fv-list .choose-play-video").removeClass("active")
			$(this).addClass('active')
			let zonename = $(this).attr('data-zonename');
			let url = $(this).attr('data-url');
			let time = $(this).attr('data-time');
			let title = $(this).attr('data-title');
			let id = $(this).attr('data-id');
			let thumb = $(this).attr('data-thumb');
			let vid = $(this).attr('data-v-vid');
			console.log(zonename, url, time, title);
			var video = document.getElementById('box-video');
			var symbol = $("#video-main")[0].src;
			$("#video-main")[0].src = 'https://www.youtube.com/embed/' + url + '?autoplay=1';

			$("#box-show-post-active .home-fv-focus home-fv-iframe iframe").attr('src', url)
			$("#box-show-post-active .home-fv-focus .home-fv-info .name").html(zonename)
			$("#box-show-post-active .home-fv-focus .home-fv-info .info .time .text").html(time)
			$("#box-show-post-active .home-fv-focus .home-fv-title").html(title)
			$("#box-show-post-active .home-fv-focus .home-fv-title").attr('title', title)
		}
	});
	window.onscroll = function () {
		var video = document.getElementById('video-main');
		if(video){
			var symbol = $("#video-main")[0].src;
			var arr = symbol.split('?');
			$("#video-main")[0].src = arr[0] + '?autoplay=1';
		}
	}

});
$('.slider-category-or').owlCarousel({
	loop: false,
	nav: true,
	dots: false,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 28,
	navText:[`<i class="fal fa-angle-left"></i>`, `<i class="fal fa-angle-right"></i>`],
	responsive: {
		0: {
			items: 1,
		},
		576: {
			items: 2,
		},
		1024: {
			items: 3,
		},
	}
});
$('.slider-top-3').owlCarousel({
	loop: false,
	nav: true,
	dots: false,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 20,
	navText:[`<i class="fal fa-angle-left"></i>`, `<i class="fal fa-angle-right"></i>`],
	responsive: {
		0: {
			items: 1,
		},
		576: {
			items: 2,
		},
		1024: {
			items: 3,
		},
		1200: {
			items: 4,
		},
	}
});
$('.slide-ads').owlCarousel({
	loop: true,
	nav: false,
	dots: true,
	autoplay: true,
	autoplayTimeout: 2000,
	autoplayHoverPause: true,
	margin: 0,
	responsive: {
		0: {
			items: 1,
		},
	}
});
$('.slider-image').owlCarousel({
	loop: true,
	nav: true,
	dots: true,
	autoplay: true,
	autoplayTimeout: 2000,
	autoplayHoverPause: true,
	margin: 30,
	navText:[`<i class="fal fa-angle-left"></i>`, `<i class="fal fa-angle-right"></i>`],
	responsive: {
		0: {
			items: 1,
		},
		390: {
			items: 2,
		},
		768: {
			items: 3,
		},
		1200: {
			items: 4,
		},
	}
});



// fix header 
var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
	var currentScrollPos = window.pageYOffset;
	if (prevScrollpos > currentScrollPos) {
		$("#header").css({
			"position": "sticky",
			"top": 0,
		});
		$("#header").addClass('active');
	} else {
		$("#header").css({
			"top": "-190px",
		});
		$("#header").removeClass('active');
	}
	prevScrollpos = currentScrollPos;
}

$(window).on('scroll', function () {
	if ($(window).scrollTop()) {
		$('#header:not(.header-project-detail-not-scroll)').addClass('active');
	} else {
		$('#header:not(.header-project-detail-not-scroll)').removeClass('active')
	};
});

function resizeImage() {
	let arrClass = [
		{ class: 'resize-image-slier-top', number: (211 / 370) },
		{ class: 'resize-image-slier-top-2', number: (198 / 353) },
		{ class: 'resize-image-blog-2', number: (307 / 570) },
		{ class: 'resize-ads-1', number: (110 / 1168) },
		{ class: 'resize-ads-2', number: (600 / 300) },
		{ class: 'resize-ads-3', number: (250 / 300) },
		{ class: 'resize-ads-4', number: (250 / 1160) },
		{ class: 'resize-image-blog-3', number: (435 / 670) },
		{ class: 'resize-album-image', number: (185 / 269) },
		{ class: 'resize-category-post', number: (198 / 353) },
		{ class: 'resize-category-post', number: (198 / 353) },
	];
	for (let i = 0; i < arrClass.length; i++) {
		if($("." + arrClass[i]['class']).length){
			let width = document.getElementsByClassName(arrClass[i]['class'])[0].offsetWidth;
			$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		}
	}
}
function resizeBoxVideo(){
	if($(".home-focus-video .home-fv-iframe iframe").length){
		let width = $('.home-focus-video .home-fv-iframe iframe').width();
		$(".home-focus-video .home-fv-iframe iframe").css('height', width * (484 / 870) + 'px');
	}
}
resizeBoxVideo()
resizeImage();
new ResizeObserver(() => {
	resizeBoxVideo();
	resizeImage();
	
}).observe(document.body)

// 
var swiper = new Swiper(".mySwiper", {
	direction: "vertical",
	slidesPerView: 1,
	spaceBetween: 30,
  //   mousewheel: true,
	navigation: {
	  nextEl: ".swiper-button-next",
	  prevEl: ".swiper-button-prev",
	},
	pagination: {
	  el: ".swiper-pagination",
	  type: "fraction",
	},
  });
var swiper = new Swiper(".swiper-item-1", {
	direction: "vertical",
	slidesPerView: 1,
	spaceBetween: 30,
  //   mousewheel: true,
	navigation: {
	  nextEl: ".swiper-button-next.cate-1",
	  prevEl: ".swiper-button-prev.cate-1",
	},
	pagination: {
	  el: ".swiper-pagination.cate-1",
	  type: "fraction",
	},
  });
var swiper = new Swiper(".swiper-item-2", {
	direction: "vertical",
	slidesPerView: 1,
	spaceBetween: 30,
  //   mousewheel: true,
	navigation: {
	  nextEl: ".swiper-button-next.cate-2",
	  prevEl: ".swiper-button-prev.cate-2",
	},
	pagination: {
	  el: ".swiper-pagination.cate-2",
	  type: "fraction",
	},
  });
var swiper = new Swiper(".swiper-item-3", {
	direction: "vertical",
	slidesPerView: 1,
	spaceBetween: 30,
  //   mousewheel: true,
	navigation: {
	  nextEl: ".swiper-button-next.cate-3",
	  prevEl: ".swiper-button-prev.cate-3",
	},
	pagination: {
	  el: ".swiper-pagination.cate-3",
	  type: "fraction",
	},
  });
var swiper = new Swiper(".swiper-item-4", {
	direction: "vertical",
	slidesPerView: 1,
	spaceBetween: 30,
  //   mousewheel: true,
	navigation: {
	  nextEl: ".swiper-button-next.cate-4",
	  prevEl: ".swiper-button-prev.cate-4",
	},
	pagination: {
	  el: ".swiper-pagination.cate-4",
	  type: "fraction",
	},
  });

  // scroll top 
	$('.scroll-top').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 500)
	});
	var offset = 600,
		$back_to_top = $('.scroll-top');
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('visible-top') : $back_to_top.removeClass('visible-top');
	});
